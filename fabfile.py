from fabric.api import *

env.use_ssh_config = True
env.hosts = ['ecommerce_production']

env.deploy_project_root = '/root/ecommerce/frontend'


def install():
	with cd('%s' % env.deploy_project_root):
		print('FETCHING UPDATES FROM GITLAB')
		run('git pull origin master')
		print('INSTALL DEPENDENCIES')
		run('npm install')
		print('BUILDING...')
		run('npm run build')

def deploy():
	install()
