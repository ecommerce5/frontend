import Vue            from 'vue'
import VueRouter      from 'vue-router'
import store          from '../store'
import Dashboard      from '../views/dashboard/Dashboard.vue'
import ListOrder      from '../views/dashboard/order/List.vue'
import ListUser       from '../views/dashboard/user/List.vue'
import CreateUser     from '../views/dashboard/user/Create.vue'
import UpdateUser     from '../views/dashboard/user/Update.vue'
import CreateProduct  from '../views/dashboard/product/Create.vue'
import UpdateProduct  from '../views/dashboard/product/Update.vue'
import ListProduct    from '../views/dashboard/product/List.vue'
import ListCategory   from '../views/dashboard/category/List.vue'
import Login          from '../views/logins/Login.vue'
import SignIn         from '../views/logins/SignIn.vue'
import ViewStatistcs  from '../views/dashboard/statistics/View.vue'
import Settings  from '../views/dashboard/settings/Settings.vue'



Vue.use(VueRouter)

const routes = [

  {
    // it is roter of dashboard 
    path: '/Dashboard',
    component: Dashboard,
    meta: { requiresAuth: true },
     children: [
       {
        path: '/listOrder',
        name: 'listOrder',
        component: ListOrder
       },
       {
         path: '',
         name: 'viewStatistcs',
         component: ViewStatistcs
       },
       {
         path: '/lists-users',
         name: 'listsUsers',
         component: ListUser
       },
       {
         path: '/create-users',
         name: 'createUsers',
         component: CreateUser
       },
       {
         path: '/update-users/:id',
         name: 'updateUsers',
         component: UpdateUser
       },
       {
         path: '/lists-products',
         name: 'listsProducts',
         component: ListProduct
       },
       {
         path: '/create-products',
         name: 'createProducts',
         component: CreateProduct
       },
       {
         path: '/update-products/:id',
         name: 'updateProducts',
         component: UpdateProduct
       },
       {
         path: '/lists-categories',
         name: 'listsCategories',
         component: ListCategory
       },
       {
         path: '/settings',
         name: 'settings',
         component: Settings
       },
     
     ],

  },
   {
     // it is roter of login of admin
     path: '/',
     component: Login,
      children: [
        {
          path: '',
          name: 'signin',
          component: SignIn,
    
        },
       
     ]
   },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from,next)=>{
    const protectedRouter = to.matched.some(record=> record.meta.requiresAuth);

  console.log('valor del token en la ruta', store.getters['user/isLoggedIn'])
  if (protectedRouter === true && store.getters['user/isLoggedIn'] === false) {
      next({name:'signin'})
    } else {
      next()
    }
})


export default router
