import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Vuelidate from 'vuelidate'
import VueSessionStorage from 'vue-sessionstorage'
import VueClipboard from 'vue-clipboard2'
 

//  import "./assets/libraryJs/vendors.bundle.js"
//  import "./assets/libraryJs/scripts.bundle"
//  import "./assets/libraryJs/datatables"
//  import "./assets/libraryJs/toucheffects"
//  import "./assets/libraryJs/modernizr.custom"
//  import "./assets/libraryJs/sweetalert2"
VueClipboard.config.autoSetContainer = true // add this line
Vue.use(VueClipboard)
Vue.use(VueSessionStorage)
Vue.use(Vuelidate)


const moment = require('moment')
require('moment/locale/es')

Vue.use(require('vue-moment'), {
  moment
})

Vue.config.productionTip = false



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
