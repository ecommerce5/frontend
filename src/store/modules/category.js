import _ from 'lodash'
import axios from 'axios'
import router from '../../router'


const url = process.env.VUE_APP_PRODUCTION ? process.env.VUE_APP_PRODUCTION : process.env.VUE_APP_TEST;


const category = {
    namespaced: true,
    state: {
        categories: [],
        status:'',
        message:'',
        category_to_edit: null,
        idBusiness: localStorage.getItem('idBusiness'),
        token: localStorage.getItem('token') || '',
        auth: {
            headers: { 'auth-token': localStorage.getItem('token') }
        }
        
    },
    actions: {

        NEW_CATEGORY: ({ commit,state }, data) => {
            return new Promise((resolve, reject) => {
                 axios.post(url + '/categories', data, state.auth)
                     .then((res) => {
                        
                        commit('CATEGORY_PROCESSED');
                         resolve(res)
                     })
                     .catch((err) => {
                         
                         commit('CATEGORY_REJECTED')
                         reject(err)
                     })
            })
        },


        LIST_CATEGORIES: ({ commit,state }) => {
           
            return new Promise((resolve, reject) => {
               
                commit('CLEAR_LIST_CATEGORIES');
                
                    axios.get(url + '/categories/list/' + state.idBusiness , state.auth)
                     .then(res => {
                         res.data.forEach(category => {
                             commit('SET_LIST_CATEGORIES', category);
                         });
                         resolve(res)
                     })
                     .catch(e => {
                         
                         reject(e)
                     })
            })
        },

        SEARCH_CATEGORY: ({ commit }, id) => {
            return new Promise((resolve, reject) => {
                try {
                    commit('GET_SEARCH_CATEGORY', id)
                    resolve()
                } catch (err) {
                    reject(err)
                }
            })
        },

        EDIT_CATEGORY: ({ commit, state }, id) => {
            return new Promise((resolve, reject) => {
                axios.get(url + '/categories/' + id, state.auth)
                    .then((res) => {
                      
                        commit('CATEGORY_EDIT', res.data);
                        resolve(res)
                    })
                    .catch((err) => {
                       
                        reject(err)
                    })
            })
        },

        UPDATE_CATEGORY: ({ commit, state}, data) => {
            
            const updateCategory = 
                {
                    "name": data.name
                }
         

            return new Promise((resolve, reject) => {
                axios.put(url + '/categories/' + data.id, updateCategory, state.auth)
                    .then((res) => {
                   
                        commit('CATEGORY_UPDATED');
                        resolve(res)
                    })
                    .catch((err) => {
                      
                        commit('CATEGORY_REJECT')
                        reject(err)
                    })
            })
        },

        DELETE_CATEGORY: ({ commit,state }, id) => {
            return new Promise((resolve, reject) => {
                axios.delete(url + '/categories/' + state.idBusiness+ '/' + id, state.auth)
                    .then(res => {
                        commit('DELETE_CATEGORY', id);
                        resolve(res)
                    })
                    .catch(e => {
                     
                        reject(e)
                    })
            })
        },

        CLEAN_STATE_CATEGORY: ({ commit }) => {
            return new Promise((resolve, reject) => {
                commit('CLEAN_STATE_CATEGORY')
                resolve()
            })
        },

        AUTH_USER_CATEGORY_LOGGED: ({ commit }) => {
            return new Promise((resolve, reject) => {
                commit('AUTH_USER_LOGGED')
                resolve()
            })
        },

        
    },
    mutations: {
        
        CATEGORY_PROCESSED(state) {
            state.status = 'success'
            state.message = 'Categoría creada con exito!!'
        },
        CATEGORY_UPDATED(state) {
            state.status  = 'success',
            state.message = 'Categoría modificada con exito!!'
        },
        CATEGORY_REJECT(state) {
            state.status = 'error'
        },

        CATEGORY_REJECTED(state) {
            state.status = 'error'
        },
        
        RESET_PROCCESS(state) {
            state.status = ''
        },

        CLEAR_LIST_CATEGORIES(state) {
            state.categories = []
        },
        SET_LIST_CATEGORIES(state, category) {
            state.categories.push(category);
        },

        GET_SEARCH_CATEGORY(state, id) {
            state.category_to_edit = _.find(state.categories, ['_id', id])
            
        },
        CATEGORY_EDIT(state, category) {
            state.category_to_edit = category
        },

        DELETE_CATEGORY(state,id) {
            _.remove(state.categories, category => category._id === id)
            state.status = 'success'
            state.message = 'Categoría Eliminada con exito!!'
        },

        CLEAN_STATE_CATEGORY(state) {

            state.categories = []
            state.category_to_edit = 'null'
            state.idBusiness = ''
            state.token = ''
        },

        AUTH_USER_LOGGED(state) {

            state.idBusiness = localStorage.getItem('idBusiness')
            state.auth = {
                headers: { 'auth-token': localStorage.getItem('token') }
            }
           
        },
    },
    getters: {
        GET_CATEGORIES(state) {
            return state.categories
        },        
    }

}
export default category