import _ from 'lodash'
import axios from 'axios'
import router from '../../router'


const url = process.env.VUE_APP_PRODUCTION ? process.env.VUE_APP_PRODUCTION : process.env.VUE_APP_TEST;


const product = {
    namespaced: true,
    state: {
        products: [],
        status: '',
        message: '',
        product_to_edit: 'null',
        images_to_edit: 'null',
        idBusiness: localStorage.getItem('idBusiness') || '',
        token: localStorage.getItem('token') || '',
        percentage: localStorage.getItem('percentage'),
        auth: {
            headers: { 'auth-token': localStorage.getItem('token') }
        }
        
    },
    actions: {
        UPDATE_PRICE: ({ commit, state }, product) => {
            let data = [
                {
                    propName: 'price',
                    value: product.price,
                }
            ]

             return new Promise((resolve, reject) => {
                axios.patch(url + '/products/' + product.id, data, state.auth)
                    .then((res) => {
                        console.log('PROD: ' + JSON.stringify( res.data ));
                        commit('PRODUCT_UPDATED_PRICE', res.data.product);
                        resolve(res)
                    })
                    .catch((err) => {
                        commit('PRODUCT_REJECT')
                        reject(err)
                    })
             })            
        },

        NEW_PRODUCT: ({ commit }, data) => {
            const formData = new FormData()

            for (var key in data)
                formData.append(key, data[key])
            
            data.files.forEach((file) => {
                formData.append('images', file.file);
            });


            return new Promise((resolve, reject) => {
                axios
                  .post(url + "/products", formData, {
                    headers: {
                      "auth-token": localStorage.getItem("token"),
                      "Content-Type": "multipart/form-data",
                    },
                  })
                  .then((res) => {
                    
                    commit("PRODUCT_PROCESSED");
                    resolve(res);
                  })
                  .catch((err) => {
                  
                    commit("PRODUCT_REJECTED");
                    reject(err);
                  });
            })
        },

        FILTER_BY_CATEGORY: ({ commit, state }, idCategory) => {
            return new Promise((resolve, reject) => {

                commit('CLEAR_LIST_PRODUCTS');
                axios.get(url + '/products/filterByCategory/' + state.idBusiness + '/' + idCategory, state.auth)
                    .then(res => {
                        res.data.forEach(product => {
                            commit('SET_LIST_PRODUCTS', product);
                        });
                        resolve(res)
                    })
                    .catch(e => {
                       
                        reject(e)
                    })
            })            
        },


        LIST_PRODUCTS: ({ commit,state }) => {
            return new Promise((resolve, reject) => {

                commit('CLEAR_LIST_PRODUCTS');
                axios.get(url + '/products/list/' + state.idBusiness, state.auth)
                    .then(res => {
                        res.data.forEach(product => {
                            commit('SET_LIST_PRODUCTS', product);
                        });
                        resolve(res)
                    })
                    .catch(e => {
                       
                        reject(e)
                    })
            })
        },

        EDIT_PRODUCT: ({ commit,state }, id) => {
            return new Promise((resolve, reject) => {
                axios.get(url + '/products/' + id, state.auth)
                    .then((res) => {
                        
                        commit('PRODUCT_EDIT', res.data);
                        resolve(res)
                    })
                    .catch((err) => {
                       
                        reject(err)
                    })
            })
        },

        EDIT_IMAGES: ({ commit,state }, id) => {
            return new Promise((resolve, reject) => {
                axios.get(url + '/products/' + id, state.auth)
                    .then((res) => {
                       
                        commit('IMAGES_EDIT', res.data);
                        resolve(res)
                    })
                    .catch((err) => {
                       
                        reject(err)
                    })
            })
        },

        

        UPDATE_PRODUCT: ({ commit, state }, data) => {

            const formData = new FormData()
          
            for (var key in data)
                formData.append(key, data[key])

            data.images.forEach((file) => {
                formData.append('images', file.file);
            });
       
             return new Promise((resolve, reject) => {
                
                 axios.put(url + '/products/' + data._id, formData, state.auth)
                     .then((res) => {
                        
                         commit('PRODUCT_UPDATED');
                         resolve(res)
                     })
                     .catch((err) => {
                        
                         commit('PRODUCT_REJECT')
                         reject(err)
                     })
             })
        },

        DELETE_PRODUCT: ({ commit,state }, id) => {
            
            return new Promise((resolve, reject) => {
                axios.delete(url + '/products/' + id, state.auth)
                    .then(res => {
                        
                        commit('DELETE_PRODUCT', id);
                        resolve(res)
                    })
                    .catch(e => {
                       
                        reject(e)
                    })
            })
        },

        DELETE_IMAGE_PRODUCT: ({ commit,state }, data) => {

            
            return new Promise((resolve, reject) => {
                

                axios.post(url + '/products/image' , data, state.auth)
                    .then(res => {
                       
                        commit('DELETE_IMAGE_PRODUCT');
                        resolve(res)
                    })
                    .catch(e => {
                       
                        reject(e)
                    })
            })
        },

        CLEAN_STATE_PRODUCT: ({ commit }) => {
            return new Promise((resolve, reject) => {
                commit('CLEAN_STATE_PRODUCT')
                resolve()
            })
        },

        AUTH_USER_PRODUCT_LOGGED: ({ commit }) => {
            return new Promise((resolve, reject) => {
                commit('AUTH_USER_LOGGED')
                resolve()
            })
        },

        


    },
    mutations: {
        PRODUCT_UPDATED_PRICE(state, product) {
            let index = _.findIndex(state.products, {_id: product._id});
            state.products.splice(index, 1, product);
        },

        PRODUCT_PROCESSED(state) {
            state.status = 'created'
            state.message = 'Producto creado con exito!!'
        },
        PRODUCT_UPDATED(state) {
            state.status = 'updated',
            state.message = 'Producto modificado con exito!!'
        },
        PRODUCT_REJECT(state) {
            state.status = 'error'
        },

        PRODUCT_REJECTED(state) {
            state.status = 'error'
        },

        RESET_PROCCESS(state) {
            state.status = ''
        },


        CLEAR_LIST_PRODUCTS(state) {
            state.products = []
        },
        SET_LIST_PRODUCTS(state, product) {
            state.products.push(product);
        },

        PRODUCT_EDIT(state, product) {
            state.product_to_edit = product
        },

        IMAGES_EDIT(state, images) {
            state.images_to_edit = images
        },

        DELETE_PRODUCT(state, id) {
            _.remove(state.products, product => product._id === id)
            state.status = 'delete'
            state.message = 'Producto Eliminado con exito!!'
        },

        DELETE_IMAGE_PRODUCT(state) {
        
            state.status = 'delete'
            state.message = 'Imagen Eliminada con exito!!'
        },

        CLEAN_STATE_PRODUCT(state) {
            state.product = []
            state.product_to_edit = 'null'
            state.idBusiness = ''
            state.token = ''
        },

        AUTH_USER_LOGGED(state) {
            state.idBusiness = localStorage.getItem('idBusiness')
            state.auth = {
                headers: { 'auth-token': localStorage.getItem('token') }
            }
        },
    },
    getters: {
        GET_PERCENTAGE: state => {
            return state.percentage/100;
        }
    }

}
export default product