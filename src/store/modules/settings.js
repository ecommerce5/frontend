import axios from 'axios'
import router from '../../router'


const url = process.env.VUE_APP_PRODUCTION ? process.env.VUE_APP_PRODUCTION : process.env.VUE_APP_TEST;


const settings = {
    namespaced: true,
    state: {
        settings: {
            delivery: true,
            mercadopago: true,
            ammount: null,
            percentage: 0,
            business_hours_morning: '',
            business_hours_afternoon: ''
        },
        idBusiness: localStorage.getItem('idBusiness'),
        token: localStorage.getItem('token') || '',
        auth: {
            headers: {
                'auth-token': localStorage.getItem('token')
            }
        }

    },
    actions: {
        SAVE_SETTINGS: ({ commit, state }, data) => {
             const updateSettings = [{
                     propName: 'delivery',
                     value: data.delivery,
                 },
                 {
                     propName: 'mercadopago',
                     value: data.mercadopago
                 },
                 {
                     propName: 'ammount',
                     value: data.ammount
                 },
                 {
                     propName: 'percentage',
                     value: data.percentage
                 },
                 {
                     propName: 'business_hours_morning',
                     value: data.business_hours_morning
                 },
                 {
                     propName: 'business_hours_afternoon',
                     value: data.business_hours_afternoon
                 },                                                   

             ]
            return new Promise((resolve, reject) => {
                axios.patch(url + '/settings/' + state.idBusiness, updateSettings, state.auth)
                    .then((res) => {
                        resolve(res)
                    })
                    .catch((err) => {
                        reject(err)
                    })
            })
        },
        FETCH_SETTINGS: ({ commit, state }, data) => {
            return new Promise((resolve, reject) => {
                axios.get(url + '/settings/' + state.idBusiness, state.auth)
                    .then((res) => {
                        commit('SET_SETTINGS', res.data);
                        resolve(res.data)
                    })
                    .catch((err) => {
                        console.log(err)
                        reject(err)
                    })
            })
        },
    },
    mutations: {
        SET_SETTINGS(state, data) {
            state.settings.delivery = data.delivery
            state.settings.mercadopago = data.mercadopago
            state.settings.ammount = data.ammount
            state.settings.percentage = data.percentaje
            state.settings.business_hours_morning = data.business_hours_morning
            state.settings.business_hours_afternoon = data.business_hours_afternoon

        },
    },
    getters: {
        GET_SETTINGS: state => {
            return state.settings
        }
    }

}
export default settings