import _ from 'lodash'
import axios from 'axios'
import router from '../../router'


const url = process.env.VUE_APP_PRODUCTION ? process.env.VUE_APP_PRODUCTION : process.env.VUE_APP_TEST;


const user = {
    namespaced: true,
    state: {
        users: [],
        emailUser: '',
        status:'',
        message:'',
        user_to_edit: 'null',
        idBusiness: localStorage.getItem('idBusiness') || '',
        loggedIn:false,
        token: localStorage.getItem('token') || '',
        auth: {
            headers: { 'auth-token': localStorage.getItem('token') }
        },
        roles: [
            { name: "Admin" },
            { name: "Editor" },
        ],
    },
    actions: {

        NEW_USER: ({ commit,state }, data) => {
            return new Promise((resolve, reject) => {
                    
                 commit('USER_IN_PROCESS')
                 axios.post(url + '/users/register', data, state.auth)
                     .then((res) => {
                        
                        commit('USER_PROCESSED');
                         resolve(res)
                     })
                     .catch((err) => {
                        
                         commit('USER_REJECTED')
                         reject(err)
                     })
            })
        },

        LOGIN_USER: ({ commit }, data) => {
            return new Promise((resolve, reject) => {
                commit('AUTH_REQUEST')
                axios.post(url + '/users/login', data)
                    .then((res) => {
                        localStorage.setItem('datos', JSON.stringify(res.data))
                        let token       = res.data['token']
                        let idBusiness  = res.data['idBusiness']
                        let email       = res.data['email']
                        let role        = res.data['role']
                        let percentage  = res.data['percentage']
                        localStorage.setItem('token', token)
                        localStorage.setItem('role', role)
                        localStorage.setItem('idBusiness', idBusiness)
                        localStorage.setItem('emailUser', email)
                        localStorage.setItem('loadedDasboard', false)
                        localStorage.setItem('percentage', percentage)
                        axios.defaults.headers.common['auth-token'] = token
                        commit('AUTH_SUCCESS', token)
                        resolve(res)
                        if (role == "Admin"){
                            router.push({ name: 'viewStatistcs' })
                        } else {
                            router.push({ name: 'listOrder' })
                        }
                    })
                    .catch((err) => {
                        var msg
                        if (err.message == 'Request failed with status code 400') {
                            msg = 'El usuario o la contraseña no son correctos ingrese nuevamente '
                        }else{
                            msg = 'Ups ocurrio un problema intente nuevamente '
                        }
                        commit('AUTH_ERROR', msg)
                        localStorage.removeItem('token')
                        reject(err)
                    })
            })
        },

        LOGOUT: ({ commit }) => {
            return new Promise((resolve, reject) => {
                localStorage.removeItem('token')
                localStorage.removeItem('idBusiness')
                localStorage.removeItem('emailUser')
                localStorage.removeItem('percentage')
                delete axios.defaults.headers.common['Authorization']
                commit('LOGOUT')
                resolve()
            })
        },

        LIST_USERS: ({ commit, state }) => {
            return new Promise((resolve, reject) => {
               
                commit('CLEAR_LIST_USERS');
                axios.get(url + '/users/list/' + state.idBusiness , state.auth)
                     .then(res => {
                         res.data.forEach(user => {
                             commit('SET_LIST_USERS', user);
                         });
                         resolve(res)
                     })
                     .catch(e => {
                         reject(e)
                     })
            })
        },

        EDIT_USER: ({ commit,state}, id) => {
            return new Promise((resolve, reject) => {
                axios.get(url + '/users/' + id, state.auth)
                    .then((res) => {
                        commit('USER_EDIT', res.data);
                        resolve(res)
                    })
                    .catch((err) => {
                       
                        reject(err)
                    })
            })
        },


        UPDATE_USER: ({ commit, state}, data) => {
            
            const updateUser = [
                {
                    propName: 'name',
                    value: data.name,
                },
                {
                    propName: 'role',
                    value: data.role,
                }
            ]

             return new Promise((resolve, reject) => {
                 commit('USER_IN_PROCESS')
                 axios.patch(url + '/users/' + data._id, updateUser, state.auth)
                     .then((res) => {
                       
                         commit('USER_UPDATED');
                         resolve(res)
                     })
                    .catch((err) => {
                      
                         commit('USER_REJECT')
                         reject(err)
                     })
             })
        },

        DELETE_USER: ({ commit,state }, id) => {
            return new Promise((resolve, reject) => {
                axios.delete(url + '/users/' + id, state.auth)
                    .then(res => {
                        commit('DELETE_USER', id);
                        resolve(res)
                    })
                    .catch(e => {
                     
                        reject(e)
                    })
            })
        },

        CLEAN_STATE_USER: ({ commit }) => {
            return new Promise((resolve, reject) => {
                commit('CLEAN_STATE_USER')
                resolve()
            })
        },

        AUTH_USER_LOGGED:({ commit }) => {
            return new Promise((resolve, reject) => {
                commit('AUTH_USER_LOGGED')
                resolve()
            })
        },
   
    },
    mutations: {
        USER_IN_PROCESS(state) {
            state.status = 'loading'
        },
        
        USER_PROCESSED(state) {
            state.status = 'created'
            state.message = 'Usuario creado con exito!!'
        },
        USER_UPDATED(state) {
            state.status  = 'updated',
            state.message = 'Usuario modificado con exito!!'
        },
        USER_REJECT(state) {
            state.status = 'error'
        },

        USER_REJECTED(state) {
            state.status = 'error'
        },
        
        RESET_PROCCESS(state) {
            state.status = ''
        },

        AUTH_REQUEST(state) {
            state.status = 'loading'
        },
        AUTH_SUCCESS(state, token) {
            state.status = 'success'
            state.token  = token
        },
        AUTH_ERROR(state, message) {
            state.status  = 'error'
            state.message = message
            
        },
        
        LOGOUT(state) {
            state.status     = 'adios'
            state.message    = 'Vuelva pronto!!'
            state.loggedIn   = false
       
            router.push({ name: 'signin' })      
        },

        CLEAR_LIST_USERS(state) {
            state.users = []
        },
        SET_LIST_USERS(state, user) {
            state.users.push(user);
        },

        USER_EDIT(state, user) {
            state.user_to_edit = user
        },

        DELETE_USER(state,id) {
            _.remove(state.users, user => user._id === id)
            state.status = 'delete'
            state.message = 'Usuario Eliminado con exito!!'
        },

        CLEAN_STATE_USER(state) {
            state.users        = []
            state.emailUser    = '' 
            state.user_to_edit = 'null'
            state.idBusiness   = ''
            state.token        = ''
            state.loggedIn     = false
            localStorage.removeItem('loadedDasboard')
            
        },

        AUTH_USER_LOGGED(state) {
            state.loggedIn = true
            state.idBusiness = localStorage.getItem('idBusiness')
            state.emailUser  = localStorage.getItem('emailUser')
            state.auth       = {
                headers: { 'auth-token': localStorage.getItem('token') }
            }
        },
    },
    getters: {
        isLoggedIn: state => !!state.token,
        isAdmin: () => {
            if (localStorage.getItem('role') == "Admin"){
                return true;
            }
            return false;
        }
    }

}
export default user