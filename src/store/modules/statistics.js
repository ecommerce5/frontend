import _ from 'lodash'
import axios from 'axios'
import router from '../../router'


const url = process.env.VUE_APP_PRODUCTION ? process.env.VUE_APP_PRODUCTION : process.env.VUE_APP_TEST;



const statistics = {
    namespaced: true,
    state: {
        salesOfTheLast7Days:'',
        ordersCategoryForPieChart: {  },
        status:'',
        message:'',
        numbersDashboard:'',
        idBusiness: localStorage.getItem('idBusiness') || '',
        auth: {
            headers: { 'auth-token': localStorage.getItem('token') }
        },
        token: localStorage.getItem('token') || '',
       
    },
    actions: {
        FETCH_ORDERS_BY_CATEGORY: ({ commit, state }) => {
            return new Promise((resolve, reject) => {
                axios.get(url + '/statistics/ordersCategoryForPieChart/' + state.idBusiness, state.auth)
                    .then((res) => {
                        commit('SET_ORDERS_BY_CATEGORY', res.data);
                        resolve(res)
                    })
                    .catch((err) => {
                        console.log(err)
                        commit('ERROR_LIST_ORDERS_7DAYS')
                        reject(err)
                    })
            })
        },


        GET_ORDERS_LAST7DAYS:({ commit, state }, data) => {
            
        
        
              return new Promise((resolve, reject) => {

                  axios.get(url + '/statistics/ordersBySevenDates/' + state.idBusiness, state.auth)
                      .then((res) => {
                    
                          commit('SET_LIST_ORDERS_7DAYS', res.data);
                          resolve(res)
                      })
                      .catch((err) => {
                          console.log(err)
                          commit('ERROR_LIST_ORDERS_7DAYS')
                          reject(err)
                      })
              })
        },

        GET_DASHBOARD_NUMBERS: ({ commit, state }, data) => {


            return new Promise((resolve, reject) => {

                axios.get(url + '/statistics/dashboardNumbers/' + state.idBusiness , state.auth)
                    .then((res) => {

                        commit('SET_DASHBOARD_NUMBERS',res.data);
                        resolve(res)
                    })
                    .catch((err) => {
                        
                        //  commit('DELIVERED_REJECT')
                        reject(err)
                    })
            })
        },

        
    },
    mutations: {
        ORDER_IN_PROCESS(state) {
            state.status = 'loading'
        },
       
        ORDER_REJECT(state) {
            state.status = 'error'
        },


        ERROR_LIST_ORDERS_7DAYS(state) {
            state.status = 'error'
        },
        SET_LIST_ORDERS_7DAYS(state, data) {
            state.salesOfTheLast7Days = data
        },

        SET_ORDERS_BY_CATEGORY(state, data) {
            state.ordersCategoryForPieChart = data
        },

        SET_DASHBOARD_NUMBERS(state, date) {
            state.numbersDashboard = date;
        },


    
        CLEAN_STATE_ORDER(state) {
            state.status = ''
            state.salesOfTheLast7Days = ''
            state.message = ''
            state.idBusiness = ''
            state.token = ''
        },
    },
    getters: {
        GET_ORDERS_BY_CATEGORY(state) {
            return state.ordersCategoryForPieChart
        }
    }

}
export default statistics