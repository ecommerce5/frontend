import _ from 'lodash'
import axios from 'axios'
import Vue from 'vue'
import router from '../../router'


const url = process.env.VUE_APP_PRODUCTION ? process.env.VUE_APP_PRODUCTION : process.env.VUE_APP_TEST;

//pusher
Vue.use(require('vue-pusher'), {
    api_key: '1da667d267a4ea3f17c3',
    options: {
        cluster: 'us2',
        encrypted: true,
    }
});

const order = {
    namespaced: true,
    state: {
        animatedIconOrders: false,
        orders: [],
        status:'',
        message:'',
        idBusiness: localStorage.getItem('idBusiness') || '',
        auth: {
            headers: { 'auth-token': localStorage.getItem('token') }
        },
        token: localStorage.getItem('token') || '',
       
    },
    actions: {


        LIST_ORDERS: ({ commit ,state}) => {
            return new Promise((resolve, reject) => {
                commit('CLEAR_LIST_ORDERS');
                axios.get(url + '/orders/list/' + state.idBusiness , state.auth)
                    .then(res => {
                        
                        res.data.forEach(order => {
                            commit('SET_LIST_ORDERS', order);
                        });
                        resolve(res)
                    })
                    .catch(e => {
                        
                        reject(e)
                    })
            })
        },

        UPDATE_DELIVERED: ({ commit, state }, data) => {
            const estimatedTime = (data.orderValue == 2 || data.orderValue == 3) 
                ? data.estimatedTime 
                : 0; 

            const updateDelivered = [
                {
                    propName: 'status',
                    value: data.status,
                    estimatedTime
                },

                {
                    propName: 'status_value',
                    value: data.orderValue
                },
                
            ]

         
            return new Promise((resolve, reject) => {
                console.log("DELIVERY -> " + JSON.stringify(updateDelivered))
                axios.patch(url + '/orders/' + data.id, updateDelivered, state.auth)
                    .then((res) => {
                       
                        commit('DELIVERED_UPDATED');
                        resolve(res)
                    })
                    .catch((err) => {
                      
                        commit('DELIVERED_REJECT')
                        reject(err)
                    })
            })
        },

        REAL_TIME_ORDERS: ({ commit }, order) => {
            commit('ORDER_ICON_ANIMATED')
        },

       

       
        
        CLEAN_STATE_ORDER: ({ commit }) => {
            return new Promise((resolve, reject) => {
                commit('CLEAN_STATE_ORDER')
                resolve()
            })
        },

        AUTH_USER_LOGGED: ({ commit }) => {
            return new Promise((resolve, reject) => {
                commit('AUTH_USER_LOGGED')
                resolve()
            })
        },

        ORDER_ICON_ANIMATED_OFF: ({ commit }) => {
            commit( 'ORDER_ICON_ANIMATED_OFF' )
        }

        
    },
    mutations: {
        ADD_NEW_ORDER(state, order) {
            let isTheOrder = _.find(state.orders, ['_id', order._id])
            if (isTheOrder == undefined) {
                state.orders.push(order)
            }
        },

        ORDER_ICON_ANIMATED(state) {
            state.animatedIconOrders = true
        },
        ORDER_ICON_ANIMATED_OFF(state) {
            state.animatedIconOrders = false
        },

        ORDER_IN_PROCESS(state) {
            state.status = 'loading'
        },
       
        ORDER_REJECT(state) {
            state.status = 'error'
        },


        CLEAR_LIST_ORDERS(state) {
            state.orders = []
        },
        SET_LIST_ORDERS(state, order) {
            state.orders.push(order);
        },

        DELIVERED_UPDATED(state) {
            state.status = 'delivered',
                
            state.message = 'El estado de la orden se a modificado con exito!!'
        },
        DELIVERED_REJECT(state) {
            state.status = 'error'
        },

        AUTH_USER_LOGGED(state) {
            
            state.idBusiness = localStorage.getItem('idBusiness')
            state.auth = {
                headers: { 'auth-token': localStorage.getItem('token') }
            }
           
        },

    
        CLEAN_STATE_ORDER(state) {
            state.status = ''
            state.orders = []
            state.message = ''
            state.idBusiness = ''
            state.token = ''
        },
    },
    getters: {
       GET_ORDER_ICON_ANIMATED(state) {
           return state.animatedIconOrders
       },
       GET_ORDERS(state) {
           return state.orders
       }
    }

}
export default order